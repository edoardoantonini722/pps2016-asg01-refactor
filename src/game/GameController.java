package game;

import elements.characters.EnemyCharacter;
import elements.characters.Mario;
import elements.objects.GameObject;
import elements.objects.Piece;
import utils.Res;

import java.util.List;

public class GameController {

    private static GameController gameController;
    private Platform platform;
    private boolean hasWon = false;
    private boolean hasLost = false;
    private Mario mario = Mario.getInstance();
    private List<GameObject> objects;
    private List<Piece> pieces;
    private List<EnemyCharacter> enemies;

    private GameController() { }

    public static GameController getInstance()
    {
        if(gameController == null)
        {
            gameController = new GameController();
        }
        return gameController;
    }

    public void playGame()
    {
        this.handlingContacts();
        this.statusCheck();
        this.platform.repaint();
    }

    public void handlingContacts()
    {
        objects = this.platform.getObjects();
        enemies = this.platform.getEnemies();
        pieces = this.platform.getPieces();
        for (GameObject object: objects) {
            if (this.mario.isNearby(object))
                this.mario.contactWithElement(object);

            for(EnemyCharacter enemy: enemies)
            {
                if (enemy.isNearby(object))
                    enemy.contactWithElement(object);
            }
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contactPiece(this.pieces.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.pieces.remove(i);
            }
        }

        for(EnemyCharacter firstEnemy : enemies)
        {
            for(EnemyCharacter secondEnemy : enemies)
            {
                if(!firstEnemy.equals(secondEnemy))
                {
                    if(firstEnemy.isNearby(secondEnemy)){
                        firstEnemy.contactWithElement(secondEnemy);
                    }
                }
            }
            if(this.mario.isNearby(firstEnemy))
            {
                this.mario.contactWithElement(firstEnemy);
            }
        }
    }

    private void statusCheck()
    {
        if(!hasWon)
        {
            hasWon = this.mario.getX() == Platform.FLAG_X_POS - platform.getXPos();
            if(hasWon)
            {
                Audio.playSound(Res.AUDIO_WIN);
            }
        }
        if(!hasLost)
        {
            hasLost = !this.mario.isAlive();
            if(hasLost)
            {
                Audio.playSound(Res.AUDIO_DIED);
            }
        }
    }

    public Platform getPlatform()
    {
        return platform;
    }

    public void setPlatform(Platform platform)
    {
        this.platform = platform;
    }



}
