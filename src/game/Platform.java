package game;

import elements.characters.*;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import elements.objects.GameObject;
import elements.objects.Piece;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    public static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    public static final int STARTING_X_POS = 0;
    public static final int STARTING_BACKGROUND1_X_POS = -50;
    public static final int STARTING_BACKGROUND2_X_POS = 750;
    private static final int STARTING_MOV = 0;
    private static final int FLOOR_OFFSET_Y = 293;
    private static final int INITIAL_SKY_LIMIT = 0;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int skyLimit;

    private PositioningStrategy positioningStrategy = DefaultPositioning.getInstance();

    private static GameController gameController = GameController.getInstance();

    private Image imgFlag;
    private Image imgCastle;

    private List<GameObject> objects;
    private List<Piece> pieces;
    private Mario mario = Mario.getInstance();
    private List<EnemyCharacter> enemies;

    public Platform() {
        super();
        this.background1PosX = STARTING_BACKGROUND1_X_POS;
        this.background2PosX = STARTING_BACKGROUND2_X_POS;
        this.mov = STARTING_MOV;
        this.xPos = STARTING_X_POS-1;
        this.floorOffsetY = FLOOR_OFFSET_Y;
        this.skyLimit = INITIAL_SKY_LIMIT;

        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);

        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        objects = new ArrayList<>();
        objects.addAll(this.positioningStrategy.getBlocks());
        objects.addAll(this.positioningStrategy.getTunnels());
        pieces = this.positioningStrategy.getPieces();

        enemies = this.positioningStrategy.getEnemies();

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
        gameController.setPlatform(this);
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getSkyLimit() {
        return skyLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getXPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setSkyLimit(int skyLimit) {
        this.skyLimit = skyLimit;
    }

    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public List<EnemyCharacter> getEnemies()
    {
        return this.enemies;
    }

    public List<GameObject> getObjects()
    {
        return this.objects;
    }

    public List<Piece> getPieces()
    {
        return this.pieces;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        staticDrawing(g);
        dynamicDrawing(g);
    }


    private void dynamicDrawing(Graphics g)
    {
        if (this.mario.isJumping())
            g.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        else
            g.drawImage(this.mario.walk(), this.mario.getX(), this.mario.getY(), null);

        for(EnemyCharacter enemy: enemies)
        {
            if (enemy.isAlive())
                g.drawImage(enemy.walk(), enemy.getX(), enemy.getY(), null);
            else
                g.drawImage(enemy.deadImage(), enemy.getX(), enemy.getY() + enemy.getDeadOffsetY(), null);

        }
    }

    private void staticDrawing(Graphics g)
    {
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).move();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).move();
            }

            for(EnemyCharacter enemy: enemies)
            {
                enemy.move();
            }
        }

        g.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g.drawImage(this.castle, 10 - this.xPos, 95, null);
        g.drawImage(this.start, 220 - this.xPos, 234, null);

        for (int i = 0; i < objects.size(); i++) {
            g.drawImage(this.objects.get(i).getObjectImage(), this.objects.get(i).getX(),
                    this.objects.get(i).getY(), null);
        }

        for (int i = 0; i < pieces.size(); i++) {
            g.drawImage(this.pieces.get(i).imageOnMovement(), this.pieces.get(i).getX(),
                    this.pieces.get(i).getY(), null);
        }

        g.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);
    }
}
