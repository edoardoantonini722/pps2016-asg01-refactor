package game;

import elements.ConcreteGameElementFactory;
import elements.GameElementFactory;
import elements.characters.EnemyCharacter;
import elements.objects.*;
import java.util.ArrayList;
import java.util.List;

public class DefaultPositioning implements PositioningStrategy {

    private List<Tunnel> tunnels = new ArrayList<>();
    private List<Block> blocks = new ArrayList<>();
    private List<Piece> pieces = new ArrayList<>();
    private List<EnemyCharacter> enemies = new ArrayList<>();
    private GameElementFactory factory = new ConcreteGameElementFactory();

    private static DefaultPositioning instance;

    private DefaultPositioning()
    {

        tunnels.add(factory.createTunnel(600, 230));
        tunnels.add(factory.createTunnel(1000, 230));
        tunnels.add(factory.createTunnel(1600, 230));
        tunnels.add(factory.createTunnel(1900, 230));
        tunnels.add(factory.createTunnel(2500, 230));
        tunnels.add(factory.createTunnel(3000, 230));
        tunnels.add(factory.createTunnel(3800, 230));
        tunnels.add(factory.createTunnel(4500, 230));

        blocks.add(factory.createBlock(400, 180));
        blocks.add(factory.createBlock(1200, 180));
        blocks.add(factory.createBlock(1270, 170));
        blocks.add(factory.createBlock(1340, 160));
        blocks.add(factory.createBlock(2000, 180));
        blocks.add(factory.createBlock(2600, 160));
        blocks.add(factory.createBlock(2650, 180));
        blocks.add(factory.createBlock(3500, 160));
        blocks.add(factory.createBlock(3550, 140));
        blocks.add(factory.createBlock(4000, 170));
        blocks.add(factory.createBlock(4200, 200));
        blocks.add(factory.createBlock(4300, 210));

        pieces.add(factory.createPiece(402, 145));
        pieces.add(factory.createPiece(1202, 140));
        pieces.add(factory.createPiece(1272, 95));
        pieces.add(factory.createPiece(1342, 40));
        pieces.add(factory.createPiece(1650, 145));
        pieces.add(factory.createPiece(2650, 145));
        pieces.add(factory.createPiece(3000, 135));
        pieces.add(factory.createPiece(3400, 125));
        pieces.add(factory.createPiece(4200, 145));
        pieces.add(factory.createPiece(4600, 40));

        enemies.add(factory.createMushroom(800,263));
        enemies.add(factory.createTurtle(950, 243));

    }

    public static PositioningStrategy getInstance()
    {
        if(instance == null)
        {
            instance = new DefaultPositioning();
        }
        return instance;
    }

    @Override
    public List<Tunnel> getTunnels() {
        return this.tunnels;
    }

    @Override
    public List<Block> getBlocks() {
        return this.blocks;
    }

    @Override
    public List<Piece> getPieces() {
        return this.pieces;
    }

    @Override
    public List<EnemyCharacter> getEnemies() {
        return this.enemies;
    }
}
