package game;

import elements.characters.Mario;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener  {

    private Mario mario = Mario.getInstance();
    private static final int PLATFORM_LEFT_LIMIT_X_POS = 0;
    private static final int PLATFORM_RIGHT_LIMIT_Y_POS = 4600;
    private static final int PLATFORM_LEFT_OVERLIMIT_X_POS = -1;
    private static final int PLATFORM_RIGHT_OVERLIMIT_Y_POS = 4601;
    private static final int PLATFORM_TO_LEFT = 1;
    private static final int PLATFORM_TO_RIGHT = -1;
    private static final int PLATFORM_STOP = 0;
    @Override
    public void keyPressed(KeyEvent e) {

        Platform platform = GameController.getInstance().getPlatform();
        if (mario.isAlive() == true) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                rightAction(platform);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                leftAction(platform);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
               upAction(platform);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        Platform platform = GameController.getInstance().getPlatform();
        mario.setMoving(false);
        platform.setMov(PLATFORM_STOP);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    private void blockCastleAndStart(Platform platform, int overLimit, int limit)
    {
        if (platform.getXPos() == overLimit) {
            platform.setXPos(limit);
            platform.setBackground1PosX(Platform.STARTING_BACKGROUND1_X_POS);
            platform.setBackground2PosX(Platform.STARTING_BACKGROUND2_X_POS);
        }
    }

    private void rightAction(Platform platform)
    {
        blockCastleAndStart(platform,PLATFORM_LEFT_OVERLIMIT_X_POS,PLATFORM_LEFT_LIMIT_X_POS);
        mario.setMoving(true);
        mario.setToRight(true);
        platform.setMov(PLATFORM_TO_LEFT);
    }

    private void upAction(Platform platform)
    {
        mario.setJumping(true);
        Audio.playSound("/resources/audio/jump.wav");
    }

    private void leftAction(Platform platform)
    {
        blockCastleAndStart(platform, PLATFORM_RIGHT_OVERLIMIT_Y_POS, PLATFORM_RIGHT_LIMIT_Y_POS);
        mario.setMoving(true);
        mario.setToRight(false);
        platform.setMov(PLATFORM_TO_RIGHT);
    }

}
