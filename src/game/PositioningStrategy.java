package game;


import elements.characters.EnemyCharacter;
import elements.objects.Block;
import elements.objects.Piece;
import elements.objects.Tunnel;

import java.util.List;

/**
 * The interface for the strategy pattern
 */

public interface PositioningStrategy {

    List<Tunnel> getTunnels();

    List<Block> getBlocks();

    List<Piece> getPieces();

    List<EnemyCharacter> getEnemies();
}
