package elements.objects;

import elements.ConcreteBasicGameElement;
import elements.GameElementWithImage;

import java.awt.Image;

public class GameObject extends ConcreteBasicGameElement implements GameElementWithImage {

    protected Image objectImage;

    public GameObject(int x, int y, int width, int height) {
        super();
        this.setHeight(height);
        this.setWidth(width);
        this.setX(x);
        this.setY(y);
    }


    @Override
    public Image getObjectImage() {
        return this.objectImage;
    }

    @Override
    public void setObjectImage(Image objectImage) {
        this.objectImage = objectImage;
    }
}
