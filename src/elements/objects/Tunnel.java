package elements.objects;

import utils.Res;
import utils.Utils;

public class Tunnel extends GameObject {

    public static final int WIDTH = 43;
    public static final int HEIGHT = 65;

    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setObjectImage(Utils.getImage(Res.IMG_TUNNEL));
    }

}
