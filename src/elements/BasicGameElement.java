package elements;

/**
 * An interface that describes the basic game element
 */

public interface BasicGameElement {

    int getWidth();

    int getHeight();

    int getX();

    int getY();

    void setWidth(int width);

    void setHeight(int height);

    void setX(int x);

    void setY(int y);

    void move();
}
