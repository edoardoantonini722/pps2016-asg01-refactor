package elements;

import game.GameController;
import game.Platform;

public abstract class ConcreteBasicGameElement implements BasicGameElement {

    private int width;
    private int height;
    private int x;
    private int y;

    public ConcreteBasicGameElement() { }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }

    public int getX()
    {
        return this.x;
    }

    public int getY()
    {
        return this.y;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public void setX(int x)
    {
        this.x=x;
    }

    public void setY(int y)
    {
        this.y=y;
    }

    public void move()
    {
        Platform platform = GameController.getInstance().getPlatform();
        if (platform.getXPos() >= Platform.STARTING_X_POS) {
            this.x = this.x - platform.getMov();
        }
    }
}
