package elements;

import java.awt.*;

/**
 * An interface for all those elemens that need to manage an image
 */
public interface GameElementWithImage {

     Image getObjectImage();

     void setObjectImage(Image objectImage);
}
