package elements;

import elements.characters.Mushroom;
import elements.characters.Turtle;
import elements.objects.Block;
import elements.objects.Piece;
import elements.objects.Tunnel;

public class ConcreteGameElementFactory implements GameElementFactory {
    @Override
    public Piece createPiece(int x, int y) {
        return new Piece(x,y);
    }

    @Override
    public Tunnel createTunnel(int x, int y) {
        return new Tunnel(x,y);
    }

    @Override
    public Block createBlock(int x, int y) {
        return new Block(x,y);
    }

    @Override
    public Turtle createTurtle(int x, int y) {
        return new Turtle(x,y);
    }

    @Override
    public Mushroom createMushroom(int x, int y) {
        return new Mushroom(x,y);
    }
}
