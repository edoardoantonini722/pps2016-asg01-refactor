package elements;

import elements.characters.Mushroom;
import elements.characters.Turtle;
import elements.objects.Block;
import elements.objects.Piece;
import elements.objects.Tunnel;

/**
 * The interface for abstract factory pattern
 */

public interface GameElementFactory {

    Piece createPiece(int x, int y);

    Tunnel createTunnel(int x, int y);

    Block createBlock(int x, int y);

    Turtle createTurtle (int x, int y);

    Mushroom createMushroom (int x, int y);
}
