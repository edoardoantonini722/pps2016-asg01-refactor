package elements.characters;

import java.awt.Image;

import elements.BasicGameElement;
import game.GameController;
import game.Platform;
import elements.objects.GameObject;
import elements.objects.Piece;
import utils.Res;
import utils.Utils;

public class Mario extends BasicCharacter {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MARIO_INIITIAL_X_POSITION = 300;
    private static final int MARIO_INITIAL_Y_POSITION = 245;
    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;
    private static final int HEIGHT_REDUCTION = 4;

    private Image objectImage;
    private boolean jumping;
    private int jumpingExtent;

    private static Mario mario;



    public static Mario getInstance()
    {
        if(mario == null)
        {
            mario = new Mario(MARIO_INIITIAL_X_POSITION,MARIO_INITIAL_Y_POSITION);
        }
        return mario;
    }


    private Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.objectImage = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String str;

        this.jumpingExtent++;
        Platform platform = GameController.getInstance().getPlatform();
        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > platform.getSkyLimit())
            {
                this.setY(this.getY() - HEIGHT_REDUCTION);
            }
            else
            {
                this.jumpingExtent = JUMPING_LIMIT;
            }
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < platform.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    public void contactWithElement(BasicGameElement element)
    {
        if(element instanceof EnemyCharacter)
        {
            this.contactEnemy((EnemyCharacter)element);
        }else if(element instanceof GameObject)
        {
            this.contactObject((GameObject)element);
        }
    }

    private void contactObject(GameObject obj) {
        Platform platform = GameController.getInstance().getPlatform();
        if (this.hitAhead(obj) && this.isToRight() || this.hitBack(obj) && !this.isToRight()) {
            platform.setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(obj) && this.jumping) {
            platform.setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(obj)) {
            platform.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(obj)) {
                platform.setSkyLimit(obj.getY() + obj.getHeight());
            } else if (!this.hitAbove(obj) && !this.jumping) {
                platform.setSkyLimit(0);
            }
        }
    }

    public boolean contactPiece(Piece piece) {
        return (this.hitBack(piece)
                || this.hitAbove(piece)
                || this.hitAhead(piece)
                || this.hitBelow(piece));
    }

    private void contactEnemy(EnemyCharacter enemyCharacter) {
        if (this.hitAhead(enemyCharacter) || this.hitBack(enemyCharacter)) {
            if (enemyCharacter.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else this.setAlive(true);
        } else if (this.hitBelow(enemyCharacter)) {
            enemyCharacter.setMoving(false);
            enemyCharacter.setAlive(false);
        }
    }


    @Override
    public Image getObjectImage() {
        return this.objectImage;
    }

    @Override
    public void setObjectImage(Image objectImage) {
        this.objectImage = objectImage;
    }

    @Override
    public String getImageName()
    {
        return Res.IMGP_CHARACTER_MARIO;
    }

    @Override
    public int getFrequency()
    {
        return MARIO_FREQUENCY;
    }

}
