package elements.characters;

import elements.BasicGameElement;
import java.awt.*;

public abstract class EnemyCharacter extends  BasicCharacter implements Runnable{

    private Image objectImage;
    private int direction;
    private static final int PAUSE = 15;
    public static final int DIRECTION_RIGHT = 1;
    public static final int DIRECTION_LEFT = -1;

    public EnemyCharacter(int x, int y, int width, int height)
    {
        super(x,y,width,height);
    }

    @Override
    public Image getObjectImage() {
        return this.objectImage;
    }

    @Override
    public void setObjectImage(Image objectImage) {
        this.objectImage = objectImage;
    }

    public void move() {
        this.direction = isToRight() ? DIRECTION_RIGHT : DIRECTION_LEFT;
        super.setX(super.getX() + this.direction);
    }


    public void contactWithElement(BasicGameElement obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.direction = DIRECTION_LEFT;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.direction = DIRECTION_RIGHT;
        }
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getDirection()
    {
        return this.direction;
    }

    public abstract int getDeadOffsetY();

    public abstract Image deadImage();

    @Override
    public void run() {
        while (true) {
            if (this.isAlive()) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
