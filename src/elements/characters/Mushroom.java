package elements.characters;

import java.awt.Image;
import utils.Res;
import utils.Utils;

public class Mushroom extends EnemyCharacter implements Runnable {

    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;
    private static final int MUSHROOM_FREQUENCY = 45;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);
        this.setDirection(DIRECTION_RIGHT);
        this.setObjectImage(Utils.getImage(Res.IMG_MUSHROOM_DEFAULT));

        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    @Override
    public String getImageName()
    {
        return Res.IMGP_CHARACTER_MUSHROOM;
    }

    @Override
    public int getFrequency()
    {
        return MUSHROOM_FREQUENCY;
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }


    @Override
    public int getDeadOffsetY()
    {
        return MUSHROOM_DEAD_OFFSET_Y;
    }
}
