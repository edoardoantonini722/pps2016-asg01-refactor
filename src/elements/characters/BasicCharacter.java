package elements.characters;

import java.awt.Image;

import elements.BasicGameElement;
import elements.ConcreteBasicGameElement;
import utils.Res;
import utils.Utils;

public abstract class BasicCharacter extends ConcreteBasicGameElement implements Character  {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int CONTACT_MARGIN = 5;
    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        this.setX(x);
        this.setY(y);
        this.setWidth(width);
        this.setHeight(height);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Image walk() {
        String str = Res.IMG_BASE + getImageName() +
                (!this.moving ||
                        ++this.counter % getFrequency() == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL)
                + (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX)
                + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public abstract String getImageName();

    public abstract int getFrequency();

    public abstract void contactWithElement(BasicGameElement element);


    protected boolean hitAhead(BasicGameElement gameElement) {
        if (this.getX() + this.getWidth() < gameElement.getX() ||
                this.getX() + this.getWidth() > gameElement.getX() + CONTACT_MARGIN ||
                this.getY() + this.getHeight() <= gameElement.getY() ||
                this.getY() >= gameElement.getY() + gameElement.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBack(BasicGameElement gameElement) {
        if (this.getX() > gameElement.getX() + gameElement.getWidth() ||
                this.getX() + this.getWidth() < gameElement.getX() + gameElement.getWidth() - CONTACT_MARGIN ||
                this.getY() + this.getHeight() <= gameElement.getY() ||
                this.getY() >= gameElement.getY() + gameElement.getHeight()) {
            return false;
        } else
            return true;
    }

    protected boolean hitBelow(BasicGameElement gameElement) {
        if (this.getX() + this.getWidth() < gameElement.getX() + CONTACT_MARGIN ||
                this.getX() > gameElement.getX() + gameElement.getWidth() - CONTACT_MARGIN ||
                this.getY() + this.getHeight() < gameElement.getY() ||
                this.getY() + this.getHeight() > gameElement.getY() + CONTACT_MARGIN) {
            return false;
        } else
            return true;
    }

    protected boolean hitAbove(BasicGameElement gameElement) {
        if (this.getX() + this.getWidth() < gameElement.getX() + CONTACT_MARGIN ||
                this.getX() > gameElement.getX() + gameElement.getWidth() - CONTACT_MARGIN ||
                this.getY() < gameElement.getY() + gameElement.getHeight() ||
                this.getY() > gameElement.getY() + gameElement.getHeight() + CONTACT_MARGIN) {
            return false;
        } else return true;
    }
    public boolean isNearby(BasicGameElement gameElement) {
        if ((this.getX() > gameElement.getX() - PROXIMITY_MARGIN &&
                this.getX() < gameElement.getX() + gameElement.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.getWidth() > gameElement.getX() - PROXIMITY_MARGIN &&
                        this.getX() + this.getWidth() < gameElement.getX() + gameElement.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }
}
