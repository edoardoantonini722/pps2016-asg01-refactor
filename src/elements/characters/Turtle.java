package elements.characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;

public class Turtle extends EnemyCharacter implements Runnable {

    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;
    private static final int TURTLE_FREQUENCY = 45;


    public Turtle(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);
        this.setDirection(DIRECTION_RIGHT);
        this.setObjectImage(Utils.getImage(Res.IMG_TURTLE_IDLE));

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
    @Override
    public String getImageName()
    {
        return Res.IMGP_CHARACTER_TURTLE;
    }

    @Override
    public int getFrequency()
    {
        return TURTLE_FREQUENCY;
    }


    @Override
    public int getDeadOffsetY()
    {
        return TURTLE_DEAD_OFFSET_Y;
    }
}
