package elements.characters;

import elements.GameElementWithImage;

import java.awt.Image;

/**
 * Interface that specifies the basic character functions
 */

public interface Character  extends GameElementWithImage {

	Image walk();

	int getCounter();

	boolean isAlive();

	boolean isMoving();

	boolean isToRight();

	void setAlive(boolean alive);

	void setMoving(boolean moving);

	void setToRight(boolean toRight);

	void setCounter(int counter);

}
